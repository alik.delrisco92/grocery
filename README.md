# Grocery List Monorepo

### Status

![Gitlab pipeline status](https://img.shields.io/gitlab/pipeline-status/alik.delrisco92/grocery?branch=main&style=for-the-badge)
![Website](https://img.shields.io/website?down_color=lightgrey&down_message=offline&label=Web&style=for-the-badge&up_color=green&up_message=online&url=http%3A%2F%2Fec2-18-224-1-253.us-east-2.compute.amazonaws.com%2Fgroceries)
![Website](https://img.shields.io/website?down_color=lightgrey&down_message=offline&label=api&style=for-the-badge&up_color=green&up_message=online&url=http%3A%2F%2Fec2-18-224-1-253.us-east-2.compute.amazonaws.com%3A3333%2Fapi%2Frest%2F)

### Tech Stack

![nx](https://img.shields.io/badge/Nx-143055?style=for-the-badge&logo=nx&logoColor=white)
![jest](https://img.shields.io/badge/Jest-C21325?style=for-the-badge&logo=jest&logoColor=white)
![eslint](https://img.shields.io/badge/Eslint-4B32C3?style=for-the-badge&logo=eslint&logoColor=white)
![prettier](https://img.shields.io/badge/Prettier-F7B93E?style=for-the-badge&logo=prettier&logoColor=white)

![typescript](https://img.shields.io/badge/Typescript-3178C6?style=for-the-badge&logo=typescript&logoColor=white)
![angular](https://img.shields.io/badge/Angular-DD0031?style=for-the-badge&logo=angular&logoColor=white)
![rest](https://img.shields.io/badge/REST-85EA2D?style=for-the-badge&logoColor=white)
![nodejs](https://img.shields.io/badge/Nodejs-339933?style=for-the-badge&logo=nodedotjs&logoColor=white)
![nestjs](https://img.shields.io/badge/Nestjs-E0234E?style=for-the-badge&logo=nestjs&logoColor=white)
![prisma](https://img.shields.io/badge/Prisma-2D3748?style=for-the-badge&logo=prisma&logoColor=white)
![swagger](https://img.shields.io/badge/Swagger-85EA2D?style=for-the-badge&logo=swagger&logoColor=black)

![docker](https://img.shields.io/badge/Docker-2496ED?style=for-the-badge&logo=docker&logoColor=white)
![aws](https://img.shields.io/badge/AWS-232F3E?style=for-the-badge&logo=amazonaws&logoColor=white)
![postgres](https://img.shields.io/badge/PostgresSQL-4169E1?style=for-the-badge&logo=postgresql&logoColor=white)
![nginx](https://img.shields.io/badge/Nginx-009639?style=for-the-badge&logo=nginx&logoColor=white)

## Getting started

### Deployed applications

The Web and Api applications are deployed in Amazon Web Services, specifically they are running in Amazon EC2.
Each time that a new change is pushed to **main** branch or a merge request is created the Gitlab CI/CD trigger the pipeline, executing multiples jobs to ensure the quality of code, that the new change doesn't break the applications running all the tests, then building the docker images and deploying to Amazon EC2

- [Gitlab CI/CD example](https://gitlab.com/alik.delrisco92/grocery/-/pipelines/466327604)

![Gitlab CI/CD Pipeline](/docs/cicd.PNG 'Gitlab CI/CD Pipeline')

- [ANGULAR WEB APPLICATION](http://ec2-18-224-1-253.us-east-2.compute.amazonaws.com/groceries)

![Angular Web Application](/docs/web.PNG 'Angular Web Application')

- [NESTJS REST API](http://ec2-18-224-1-253.us-east-2.compute.amazonaws.com:3333/api/rest/)

![Nestjs REST Api](/docs/api.PNG 'Nestjs REST Api')

### Local Development Environment Setup

#### Requirements

- **Nodejs**
- **npm**
- **docker**

#### Install npm dependencies and setup husky

```bash
$ npm install
$ npm run prepare-husky
```

#### Databases setup

```bash
$ docker-compose up -d

# can create the .env and .env.test files manually and set the variables in the case that the next 2 commands failed
$ echo DATABASE_URL="postgresql://postgres:postgres@localhost:5432/grocery?schema=public" > .env
$ echo DATABASE_URL="postgresql://postgres:postgres_test@localhost:5433/postgres_test?schema=public" > .env.test

$ npm run migrate
```

#### Startup the applications in watch mode

```bash
$ npm run serve:api # http://localhost:3333/api/rest/
$ npm run serve:web # http://localhost:4200
```

#### Run tests

```bash
$ npm run test:web
$ npm run e2e:api
```

## Improvements

### 1. Improve UI/UX

### 2. Provide PWA and Mobile versions

### 3. Offline support

Cache path **/groceries** using Service Workers.

Store in the applications changes history and when the app goes online send all of them to the API.

### 4. Deploy the Web and Api over Https

### 5. Multiple groceries list support

### 6. Add tools for improving Observability and Monitoring

Like Sentry, Jaeger, Grafana, etc

### 7. Add multiples languages

Add more translations in differents languages like: **apps\grocery\src\assets\i18n\en.json**.
Change web app language at runtime.

### 8. Add e2e tests in web application using Cypress

### 9. Increase Code Coverage

Add Code Coverage jobs in the CI/CD

### 10. Improve CI/CD for supporting multiple environments

Like Develop, QA, Production

### 11. Add Authentication and Authorization

Implement **Sharing the groceries lists** feature using Authentication and Authorization
Implement Authentication through several Identity Providers

### 12. Scalability

Using the info provided by Observability and Monitoring tools we can scale the application horizontally and vertically.
Increasing the hardware resources of the AWS EC2 Instance or
improving Continues Deployment process deploying the containerized in solutions like Aws Fargate or Kubernetes

### 13. Error handling support in Web and Api applications

Handle exceptions produced by bad http requests or internal errors and store the logs properly(Sentry) to be analized later.

### 14. Use lightweight databases for running Api e2e, like SQLite or H2

## Stay in touch

- Author - [Alik del Risco](https://alikdelrisco.github.io/cv/)
