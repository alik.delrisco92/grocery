export class Grocery {
  id: string;
  name: string;
  purchased: boolean;
  createdAt: Date;
  updatedAt: Date;
}
