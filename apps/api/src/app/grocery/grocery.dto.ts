import { Grocery } from '@grocery/api-interfaces';
import { PickType } from '@nestjs/swagger';

export class GroceryCreateDto extends PickType(Grocery, ['name'] as const) { }
export class GroceryUpdateDto extends PickType(Grocery, ['name', 'purchased'] as const) { }
