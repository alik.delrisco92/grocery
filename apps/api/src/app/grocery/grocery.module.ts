import { Module } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { GroceryController } from './grocery.controller';
import { GroceryPrismaRepo } from './grocery.prisma-repo';
import { GroceryRepoToken } from './grocery.repo';

@Module({
    controllers: [GroceryController],
    providers: [
        PrismaService,
        { provide: GroceryRepoToken, useClass: GroceryPrismaRepo }
    ]
})
export class GroceryModule {}
