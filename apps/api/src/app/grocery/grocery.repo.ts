import { Grocery } from '@grocery/api-interfaces';
import { GroceryCreateDto, GroceryUpdateDto } from './grocery.dto';

export interface GroceryRepo {
    create(dto: GroceryCreateDto): Promise<Grocery>
    findAll(): Promise<Grocery[]>
    deleteById(id: string): Promise<void>
    update(id: string, dto: GroceryUpdateDto): Promise<Grocery>
}
export const GroceryRepoToken = 'GroceryRepo'
