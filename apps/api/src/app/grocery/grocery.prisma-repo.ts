import { Grocery } from '@grocery/api-interfaces';
import { Injectable } from '@nestjs/common';
import { PrismaService } from '../prisma.service';
import { GroceryCreateDto, GroceryUpdateDto } from './grocery.dto';
import { GroceryRepo } from './grocery.repo';

@Injectable()
export class GroceryPrismaRepo implements GroceryRepo {
  constructor(private readonly prisma: PrismaService) {}

  async create(dto: GroceryCreateDto): Promise<Grocery> {
    return await this.prisma.grocery.create({ data: dto });
  }

  async findAll(): Promise<Grocery[]> {
    return await this.prisma.grocery.findMany({
      orderBy: { createdAt: 'asc' },
    });
  }

  async deleteById(id: string): Promise<void> {
    await this.prisma.grocery.delete({ where: { id } });
  }

  async update(id: string, dto: GroceryUpdateDto): Promise<Grocery> {
    return await this.prisma.grocery.update({ data: dto, where: { id } });
  }
}
