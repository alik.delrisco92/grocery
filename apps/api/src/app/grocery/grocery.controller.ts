import { Grocery } from '@grocery/api-interfaces';
import { Body, Controller, Delete, Get, HttpCode, Inject, Param, Patch, Post } from '@nestjs/common';
import { GroceryCreateDto, GroceryUpdateDto } from './grocery.dto';
import { GroceryRepo, GroceryRepoToken } from './grocery.repo';

@Controller({ path: 'groceries', version: '1' })
export class GroceryController {
    constructor(@Inject(GroceryRepoToken) private readonly repo: GroceryRepo) { }

    @HttpCode(201)
    @Post()
    async create(@Body() dto: GroceryCreateDto): Promise<Grocery> {
        return await this.repo.create(dto);
    }

    @HttpCode(200)
    @Get()
    async findAll(): Promise<Grocery[]> {
        return await this.repo.findAll();
    }

    @HttpCode(204)
    @Delete('/:id')
    async deleteById(@Param('id') id: string): Promise<void> {
        await this.repo.deleteById(id);
    }

    @HttpCode(200)
    @Patch('/:id')
    async update(
        @Param('id') id: string,
        @Body() dto: GroceryUpdateDto
    ): Promise<Grocery> {
        return await this.repo.update(id, dto);
    }
}