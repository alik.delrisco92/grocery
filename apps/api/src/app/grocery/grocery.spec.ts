import * as request from 'supertest';
import { Test } from '@nestjs/testing';
import { GroceryModule } from './grocery.module';
import { INestApplication } from '@nestjs/common';
import { PrismaService } from '../prisma.service';

describe('Grocery', () => {
  let app: INestApplication;
  let prisma: PrismaService;

  const cleanGroceryTable = () => prisma.grocery.deleteMany();

  beforeAll(async () => {
    const moduleRef = await Test.createTestingModule({
      imports: [GroceryModule],
    }).compile();

    app = moduleRef.createNestApplication();
    await app.init();
    prisma = app.get<PrismaService>(PrismaService);
    await cleanGroceryTable();
  });

  beforeEach(async () => {
    await cleanGroceryTable();
  });

  afterAll(async () => {
    await cleanGroceryTable();
    await app.close();
  });

  describe('/POST /groceries', () => {
    it('should return 201 status code', async () => {
      const response = await request(app.getHttpServer())
        .post('/groceries')
        .send({ name: 'bread' });
      expect(response.status).toBe(201);
    });
    it('should be not purchased', async () => {
      const response = await request(app.getHttpServer())
        .post('/groceries')
        .send({ name: 'bread' });
      expect(response.body.purchased).toBe(false);
    });
    it('should return the same name', async () => {
      const response = await request(app.getHttpServer())
        .post('/groceries')
        .send({ name: 'bread' });
      expect(response.body.name).toBe('bread');
    });
    it('should store it in the database', async () => {
      await cleanGroceryTable();
      await request(app.getHttpServer())
        .post('/groceries')
        .send({ name: 'bread' });
      const groceries = await prisma.grocery.findMany({
        where: { name: { equals: 'bread' } },
      });
      expect(groceries.length).toBe(1);
      expect(groceries[0].name).toBe('bread');
    });
  });

  describe('/GET /groceries', () => {
    beforeEach(async () => {
      await prisma.grocery.create({ data: { name: 'bread' } });
      await prisma.grocery.create({ data: { name: 'apple' } });
      await prisma.grocery.create({ data: { name: 'eggs' } });
    });
    it('should return 200 status code', async () => {
      const response = await request(app.getHttpServer()).get('/groceries');
      expect(response.status).toEqual(200);
    });

    it('should return an array with the existing groceries', async () => {
      const response = await request(app.getHttpServer()).get('/groceries');
      expect(response.body.length).toBe(3);
      expect(response.body).toEqual(
        expect.arrayContaining([
          expect.objectContaining({ name: 'apple' }),
          expect.objectContaining({ name: 'bread' }),
          expect.objectContaining({ name: 'eggs' }),
        ])
      );
    });
    it('should return an array with the existing groceries sorted asc by createdAt', async () => {
      const response = await request(app.getHttpServer()).get('/groceries');
      expect(response.body[0].name).toBe('bread');
      expect(response.body[1].name).toBe('apple');
      expect(response.body[2].name).toBe('eggs');
    });
  });

  describe('/DELETE /groceries/:id', () => {
    let groceryId;
    beforeEach(async () => {
      const grocery = await prisma.grocery.create({ data: { name: 'bread' } });
      groceryId = grocery.id;
    });
    it('should return 204 status code', async () => {
      const response = await request(app.getHttpServer()).delete(
        `/groceries/${groceryId}`
      );
      expect(response.status).toEqual(204);
    });
    it('should delete the grocery from the database', async () => {
      await request(app.getHttpServer()).delete(`/groceries/${groceryId}`);
      const data = await prisma.grocery.findUnique({
        where: { id: groceryId },
      });
      expect(data).toBeNull();
    });
  });

  describe('/PATCH /groceries/:id', () => {
    let groceryId;
    beforeEach(async () => {
      const grocery = await prisma.grocery.create({ data: { name: 'bread' } });
      groceryId = grocery.id;
    });

    it('should return 200 status code', async () => {
      const response = await request(app.getHttpServer())
        .patch(`/groceries/${groceryId}`)
        .send({ purchased: true, name: 'cookies' });

      expect(response.status).toEqual(200);
    });

    it('should return the updated grocery', async () => {
      const response = await request(app.getHttpServer())
        .patch(`/groceries/${groceryId}`)
        .send({ purchased: true, name: 'cookies' });

      expect(response.body.name).toEqual('cookies');
      expect(response.body.purchased).toEqual(true);
    });
    it('should update the grocery from the database 200', async () => {
      await request(app.getHttpServer())
        .patch(`/groceries/${groceryId}`)
        .send({ purchased: true, name: 'cookies' });

      const grocery = await prisma.grocery.findUnique({
        where: { id: groceryId },
      });
      expect(grocery.name).toEqual('cookies');
      expect(grocery.purchased).toEqual(true);
    });
  });
});
