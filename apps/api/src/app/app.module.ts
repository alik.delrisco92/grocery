import { Module } from '@nestjs/common';
import { GroceryModule } from './grocery';

@Module({
  imports: [GroceryModule],
})
export class AppModule {}
