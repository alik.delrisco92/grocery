import { ReactiveFormsModule } from '@angular/forms';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';

import { MaterialModule } from '../../../material.module';
import { CreateGroceryComponent } from './create-grocery.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { GroceryFacade } from '../grocery.facade';
import { APP_CONFIG, appConfig } from '../../../app.config';

describe('CreateGroceryComponent', () => {
  let component: CreateGroceryComponent;
  let fixture: ComponentFixture<CreateGroceryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
      ],
      providers: [
        GroceryFacade,
        {
          provide: APP_CONFIG,
          useValue: appConfig,
        },
      ],
      declarations: [CreateGroceryComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CreateGroceryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
