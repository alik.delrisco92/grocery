import { ChangeDetectionStrategy, Component, OnDestroy } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { Subscription } from 'rxjs';
import { GroceryFacade } from '../grocery.facade';

@Component({
  selector: 'grocery-create',
  templateUrl: './create-grocery.component.html',
  styles: ['.field { flex: 1; } .form { display: flex; flex: 1; }'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateGroceryComponent implements OnDestroy {
  form = this.fb.group({
    name: [null, Validators.required],
  });
  subscription: Subscription;

  constructor(private facade: GroceryFacade, private fb: FormBuilder) {}

  create() {
    this.subscription = this.facade.create(this.form.value).subscribe(() => {
      this.form.reset();
    });
  }

  ngOnDestroy(): void {
    this.subscription && this.subscription.unsubscribe();
  }
}
