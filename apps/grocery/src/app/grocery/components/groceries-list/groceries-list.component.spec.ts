import { ComponentFixture, TestBed } from '@angular/core/testing';
import { NoopAnimationsModule } from '@angular/platform-browser/animations';
import { ReactiveFormsModule } from '@angular/forms';

import { MaterialModule } from '../../../material.module';
import { GroceriesListComponent } from './groceries-list.component';
import { GroceryFacade } from '../grocery.facade';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { APP_CONFIG, appConfig } from '../../../app.config';
import { of } from 'rxjs';

const MockGroceryFacade = {
  delete: jest.fn().mockReturnValue(of(null)),
  update: jest
    .fn()
    .mockReturnValue(of({ id: '1', name: 'bread', purchased: true })),
};

describe('GroceriesListComponent', () => {
  let component: GroceriesListComponent;
  let fixture: ComponentFixture<GroceriesListComponent>;
  let facade: GroceryFacade;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        MaterialModule,
        NoopAnimationsModule,
        ReactiveFormsModule,
        HttpClientTestingModule,
      ],
      providers: [
        {
          provide: GroceryFacade,
          useValue: MockGroceryFacade,
        },
        {
          provide: APP_CONFIG,
          useValue: appConfig,
        },
      ],
      declarations: [GroceriesListComponent],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroceriesListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
    facade = TestBed.inject(GroceryFacade);
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('delete', () => {
    it('should call delete method in facade with groceryId', () => {
      component.delete('1');
      expect(facade.delete).toHaveBeenCalledWith('1');
    });
  });

  describe('update', () => {
    it('should call update method in facade with grocery update dto', () => {
      component.update({
        id: '1',
        name: 'bread',
        purchased: true,
      });
      expect(facade.update).toHaveBeenCalledWith({
        id: '1',
        name: 'bread',
        purchased: true,
      });
    });
  });
});
