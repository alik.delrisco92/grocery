import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
} from '@angular/core';
import { Subscription } from 'rxjs';
import { GroceryFacade, GroceryUpdateDto } from '../grocery.facade';
import { Grocery } from '@grocery/api-interfaces';

@Component({
  selector: 'grocery-list',
  templateUrl: './groceries-list.component.html',
  styleUrls: ['./groceries-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GroceriesListComponent implements OnDestroy {
  @Input() groceries: Grocery[];

  subscription: Subscription = new Subscription();

  constructor(public facade: GroceryFacade) {}

  update(dto: GroceryUpdateDto) {
    this.subscription.add(this.facade.update(dto).subscribe());
  }

  delete(id: string) {
    this.subscription.add(this.facade.delete(id).subscribe());
  }

  ngOnDestroy(): void {
    this.subscription.unsubscribe();
  }
}
