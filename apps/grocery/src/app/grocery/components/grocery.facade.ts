import { Inject, Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Grocery } from '@grocery/api-interfaces';
import { BehaviorSubject, Observable, tap } from 'rxjs';
import { AppConfig, APP_CONFIG } from '../../app.config';

export interface GroceryState {
  groceries: Grocery[];
  notPurchasedGroceries: Grocery[];
  purchasedGroceries: Grocery[];
}

let _state: GroceryState = {
  groceries: [],
  purchasedGroceries: [],
  notPurchasedGroceries: [],
};

export interface GroceryUpdateDto {
  id: string;
  name?: string;
  purchased?: boolean;
}
@Injectable()
export class GroceryFacade {
  private readonly resourceUrl: string;
  private store = new BehaviorSubject<GroceryState>(_state);
  private state$ = this.store.asObservable();

  vm$: Observable<GroceryState> = this.state$;

  constructor(
    private readonly http: HttpClient,
    @Inject(APP_CONFIG) private config: AppConfig
  ) {
    this.resourceUrl = `${this.config.apiUrl}/groceries`;
    this.findAll().subscribe((groceries) => this.updateState({ groceries }));
  }

  private updateState({ groceries }: Pick<GroceryState, 'groceries'>) {
    const notPurchasedGroceries = groceries.filter(
      (grocery) => !grocery.purchased
    );
    const purchasedGroceries = groceries.filter((grocery) => grocery.purchased);
    this.store.next(
      (_state = { groceries, notPurchasedGroceries, purchasedGroceries })
    );
  }

  private findAll(): Observable<Grocery[]> {
    return this.http.get<Grocery[]>(this.resourceUrl);
  }

  create(dto: { name: string }): Observable<Grocery> {
    return this.http
      .post<Grocery>(this.resourceUrl, dto)
      .pipe(
        tap((grocery) =>
          this.updateState({ groceries: [..._state.groceries, grocery] })
        )
      );
  }

  delete(id: string) {
    return this.http.delete<Grocery>(`${this.resourceUrl}/${id}`).pipe(
      tap(() => {
        const existingGroceries = _state.groceries.filter(
          (grocery) => grocery.id !== id
        );
        this.updateState({ groceries: existingGroceries });
      })
    );
  }

  update({ id, ...dto }: GroceryUpdateDto) {
    return this.http.patch<Grocery>(`${this.resourceUrl}/${id}`, dto).pipe(
      tap((updatedGrocery) => {
        const updatedGroceryIndex = _state.groceries.findIndex(
          (grocery) => grocery.id === id
        );
        const groceries = [..._state.groceries];
        groceries[updatedGroceryIndex] = updatedGrocery;
        this.updateState({ groceries });
      })
    );
  }
}
