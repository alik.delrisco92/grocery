import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GroceriesComponent } from './groceries.component';
import { GroceryFacade } from '../grocery.facade';
import { APP_CONFIG, appConfig } from '../../../app.config';
import { HttpClientTestingModule } from '@angular/common/http/testing';

describe('GroceriesComponent', () => {
  let component: GroceriesComponent;
  let fixture: ComponentFixture<GroceriesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [GroceriesComponent],
      providers: [
        GroceryFacade,
        {
          provide: APP_CONFIG,
          useValue: appConfig,
        },
      ],
    }).compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GroceriesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
