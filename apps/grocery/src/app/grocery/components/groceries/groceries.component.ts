import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { GroceryFacade, GroceryState } from '../grocery.facade';

@Component({
  selector: 'grocery-groceries',
  templateUrl: './groceries.component.html',
  styleUrls: ['./groceries.component.scss'],
})
export class GroceriesComponent {
  vm$: Observable<GroceryState> = this.facade.vm$;

  constructor(public facade: GroceryFacade) {}
}
