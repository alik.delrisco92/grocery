import { TestBed } from '@angular/core/testing';

import { GroceryFacade } from './grocery.facade';
import { APP_CONFIG, appConfig } from '../../app.config';
import { Grocery } from '@grocery/api-interfaces';
import { HttpClient } from '@angular/common/http';
import { of } from 'rxjs';

const groceries: Grocery[] = [
  {
    id: '1',
    name: 'bread',
    createdAt: new Date(),
    updatedAt: new Date(),
    purchased: false,
  },
  {
    id: '2',
    name: 'eggs',
    createdAt: new Date(),
    updatedAt: new Date(),
    purchased: true,
  },
  {
    id: '3',
    name: 'apple',
    createdAt: new Date(),
    updatedAt: new Date(),
    purchased: true,
  },
];

const MockHttpClient = {
  get() {
    return of(groceries);
  },
  post() {
    return of({ id: '5', name: 'chocolate', purchased: false });
  },
};

describe('GroceryFacade', () => {
  let facade: GroceryFacade;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [],
      providers: [
        GroceryFacade,
        {
          provide: APP_CONFIG,
          useValue: appConfig,
        },
        {
          provide: HttpClient,
          useValue: MockHttpClient,
        },
      ],
    });
    facade = TestBed.inject(GroceryFacade);
  });

  it('should be created', () => {
    expect(facade).toBeTruthy();
  });

  describe('updateState', () => {
    it('should emit the new state with all the groceries', (done) => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      facade.updateState({ groceries });
      facade.vm$.subscribe(({ groceries: stateGroceries }) => {
        expect(stateGroceries).toEqual(groceries);
        done();
      });
    });
    it('should emit the new state with the purchasedGroceries', (done) => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      facade.updateState({ groceries });
      facade.vm$.subscribe(({ purchasedGroceries }) => {
        expect(purchasedGroceries.length).toBe(2);
        expect(purchasedGroceries[0].id).toBe('2');
        expect(purchasedGroceries[1].id).toBe('3');
        done();
      });
    });
    it('should emit the new state with the not purchasedGroceries', (done) => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      facade.updateState({ groceries });
      facade.vm$.subscribe(({ notPurchasedGroceries }) => {
        expect(notPurchasedGroceries.length).toBe(1);
        expect(notPurchasedGroceries[0].id).toBe('1');
        done();
      });
    });
  });

  describe('findAll', () => {
    it('should return all the groceries', (done) => {
      // eslint-disable-next-line @typescript-eslint/ban-ts-comment
      // @ts-ignore
      facade.findAll().subscribe((fetchedGroceries) => {
        expect(fetchedGroceries.length).toBe(3);
        expect(fetchedGroceries[0].id).toBe('1');
        expect(fetchedGroceries[1].id).toBe('2');
        expect(fetchedGroceries[2].id).toBe('3');
        done();
      });
    });
  });

  describe('create', () => {
    it('should return the created grocery', (done) => {
      facade.create({ name: 'chocolate' }).subscribe((createdGrocery) => {
        expect(createdGrocery).toEqual({
          id: '5',
          name: 'chocolate',
          purchased: false,
        });
        done();
      });
    });
    it('should emit a new state with the created grocery', (done) => {
      facade.create({ name: 'chocolate' }).subscribe();
      facade.vm$.subscribe((state) => {
        expect(state.groceries).toEqual(
          expect.arrayContaining([
            expect.objectContaining({
              id: '5',
              name: 'chocolate',
              purchased: false,
            }),
          ])
        );
        done();
      });
    });
  });
});
