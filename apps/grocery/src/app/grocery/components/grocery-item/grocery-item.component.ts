import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl } from '@angular/forms';
import { Grocery } from '@grocery/api-interfaces';
import { GroceryUpdateDto } from '../grocery.facade';
import { debounceTime, distinctUntilChanged, Subscription } from 'rxjs';

@Component({
  selector: 'grocery-item',
  templateUrl: './grocery-item.component.html',
  styleUrls: ['./grocery-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GroceryItemComponent implements OnInit, OnDestroy {
  @ViewChild('input') input: ElementRef;

  @Input() grocery: Grocery;
  @Output() update = new EventEmitter<GroceryUpdateDto>();
  @Output() delete = new EventEmitter<string>();

  name: FormControl;
  subscription: Subscription;

  ngOnInit(): void {
    this.name = this.buildNameTermControl(this.grocery.id, this.grocery.name);
  }

  buildNameTermControl(id: string, initialName: string): FormControl {
    const name = new FormControl(initialName);
    this.subscription = name.valueChanges
      .pipe(debounceTime(300), distinctUntilChanged())
      .subscribe((newName) => {
        this.update.emit({ id, name: newName });
      });
    return name;
  }

  togglePurchase({ id, purchased }: Grocery) {
    this.update.emit({ id, purchased: !purchased });
  }

  deleteGrocery(id: string) {
    this.delete.emit(id);
  }

  ngOnDestroy(): void {
    this.subscription && this.subscription.unsubscribe();
  }
}
