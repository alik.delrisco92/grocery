import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material.module';
import { TranslocoRootModule } from '../transloco-root.module';

import { GroceriesListComponent } from './components/groceries-list/groceries-list.component';
import { GroceryItemComponent } from './components/grocery-item/grocery-item.component';
import { GroceryFacade } from './components/grocery.facade';
import { CreateGroceryComponent } from './components/create-grocery/create-grocery.component';
import { GroceriesComponent } from './components/groceries/groceries.component';

const routes: Routes = [
  {
    path: '',
    component: GroceriesComponent,
  },
];

@NgModule({
  declarations: [
    GroceriesListComponent,
    GroceryItemComponent,
    CreateGroceryComponent,
    GroceriesComponent,
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule.forChild(routes),
    MaterialModule,
    TranslocoRootModule,
  ],
  providers: [GroceryFacade],
})
export class GroceryModule {}
