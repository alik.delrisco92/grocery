import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { TranslocoRootModule } from './transloco-root.module';
import { appConfig, APP_CONFIG } from './app.config';

const AngularModules = [BrowserModule, HttpClientModule, BrowserAnimationsModule];
const ThirdPartyModules = [TranslocoRootModule]
const AppModules = [AppRoutingModule];


@NgModule({
  declarations: [AppComponent],
  imports: [
    ...AngularModules,
    ...AppModules,
    ...ThirdPartyModules
  ],
  providers: [
    {
      provide: APP_CONFIG, useValue: appConfig
    }
  ],
  bootstrap: [AppComponent],
})
export class AppModule { }
