import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router'; // CLI imports router

const routes: Routes = [
    { path: 'groceries', loadChildren: () => import('./grocery/grocery.module').then(m => m.GroceryModule) },
    { path: '', redirectTo: '/groceries', pathMatch: 'full' },
    { path: '**', redirectTo: '/groceries', },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }
