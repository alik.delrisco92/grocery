FROM node:16-alpine
WORKDIR /app
RUN npm install nx -g
COPY package*.json ./
COPY decorate-angular-cli.js ./
RUN npm install && npm cache clean --force
ENV PATH /app/node_modules/.bin:$PATH
